PGDMP     !        
            w            DBTestAutomation    10.9    11.3 I    v           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            w           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            x           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            y           1262    16393    DBTestAutomation    DATABASE     �   CREATE DATABASE "DBTestAutomation" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
 "   DROP DATABASE "DBTestAutomation";
             postgres    false            �            1259    16505 
   tb_browser    TABLE     h   CREATE TABLE public.tb_browser (
    browser_id integer NOT NULL,
    browser character varying(255)
);
    DROP TABLE public.tb_browser;
       public         postgres    false            �            1259    16510    tb_browser_browser_id_seq    SEQUENCE     �   ALTER TABLE public.tb_browser ALTER COLUMN browser_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_browser_browser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    215            �            1259    16394 
   tb_project    TABLE     �   CREATE TABLE public.tb_project (
    proj_id integer NOT NULL,
    proj_name character varying(255),
    proj_description character varying(255),
    client character varying(255)
);
    DROP TABLE public.tb_project;
       public         postgres    false            �            1259    16400    tb_project_ID_seq    SEQUENCE     �   ALTER TABLE public.tb_project ALTER COLUMN proj_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."tb_project_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    196            �            1259    16402    tb_project_status    TABLE     {   CREATE TABLE public.tb_project_status (
    proj_status_id integer NOT NULL,
    proj_id integer,
    status_id integer
);
 %   DROP TABLE public.tb_project_status;
       public         postgres    false            �            1259    16405 $   tb_project_status_proj_status_id_seq    SEQUENCE     �   ALTER TABLE public.tb_project_status ALTER COLUMN proj_status_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_project_status_proj_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    198            �            1259    16476 	   tb_status    TABLE     �   CREATE TABLE public.tb_status (
    status_id integer NOT NULL,
    status character varying(255) NOT NULL,
    definition character varying(255)
);
    DROP TABLE public.tb_status;
       public         postgres    false            �            1259    16481    tb_status_status_id_seq    SEQUENCE     �   ALTER TABLE public.tb_status ALTER COLUMN status_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_status_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    211            �            1259    16407    tb_test_collection    TABLE     �   CREATE TABLE public.tb_test_collection (
    "ID" integer NOT NULL,
    test_scenario_id character varying(255),
    test_scenario character varying(255)
);
 &   DROP TABLE public.tb_test_collection;
       public         postgres    false            �            1259    16413    tb_test_collection_result    TABLE     g  CREATE TABLE public.tb_test_collection_result (
    "ID" integer NOT NULL,
    test_scenario_id character varying(255),
    test_cycle_id integer,
    test_module_id character varying(20),
    project_id integer,
    status_id integer,
    start_datetime character varying(150),
    end_datetime character varying(150),
    duration character varying(150)
);
 -   DROP TABLE public.tb_test_collection_result;
       public         postgres    false            �            1259    16419    tb_test_collection_ID_seq    SEQUENCE     �   ALTER TABLE public.tb_test_collection_result ALTER COLUMN "ID" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."tb_test_collection_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    201            �            1259    16516    tb_test_collection_ID_seq1    SEQUENCE     �   ALTER TABLE public.tb_test_collection ALTER COLUMN "ID" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."tb_test_collection_ID_seq1"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    200            �            1259    16421    tb_test_cycle    TABLE     �  CREATE TABLE public.tb_test_cycle (
    test_cycle_id integer NOT NULL,
    test_cycle_name character varying(150),
    test_cycle_description character varying(255),
    test_duration character varying(50),
    tester_id integer,
    env_id character varying(50),
    testing_id character varying(50),
    browser_id character varying(50),
    browser_version character varying(100),
    start_datetime character varying(100),
    end_datetime character varying(100)
);
 !   DROP TABLE public.tb_test_cycle;
       public         postgres    false            �            1259    16427    tb_test_cycle_test_cycle_id_seq    SEQUENCE     �   ALTER TABLE public.tb_test_cycle ALTER COLUMN test_cycle_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_test_cycle_test_cycle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    203            �            1259    16492    tb_test_environment    TABLE     v   CREATE TABLE public.tb_test_environment (
    env_id integer NOT NULL,
    test_environment character varying(255)
);
 '   DROP TABLE public.tb_test_environment;
       public         postgres    false            �            1259    16563    tb_test_environment_env_id_seq    SEQUENCE     �   ALTER TABLE public.tb_test_environment ALTER COLUMN env_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_test_environment_env_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    214            �            1259    16429    tb_test_module    TABLE     �   CREATE TABLE public.tb_test_module (
    "ID" integer NOT NULL,
    module_id character varying(255),
    module_name character varying(255)
);
 "   DROP TABLE public.tb_test_module;
       public         postgres    false            �            1259    16435    tb_test_module_result    TABLE     .  CREATE TABLE public.tb_test_module_result (
    "ID" integer NOT NULL,
    module_id character varying(20),
    test_cycle_id integer,
    proj_id integer,
    status_id integer,
    start_datetime character varying(150),
    end_datetime character varying(150),
    duration character varying(150)
);
 )   DROP TABLE public.tb_test_module_result;
       public         postgres    false            �            1259    16438    tb_test_module_ID_seq    SEQUENCE     �   ALTER TABLE public.tb_test_module_result ALTER COLUMN "ID" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."tb_test_module_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    206            �            1259    16514    tb_test_module_ID_seq1    SEQUENCE     �   ALTER TABLE public.tb_test_module ALTER COLUMN "ID" ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."tb_test_module_ID_seq1"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    205            �            1259    16440    tb_test_step    TABLE     �   CREATE TABLE public.tb_test_step (
    id integer NOT NULL,
    test_step_id character varying(255),
    test_step_description character varying(255),
    test_value character varying(255)
);
     DROP TABLE public.tb_test_step;
       public         postgres    false            �            1259    16565    tb_test_step_id_seq    SEQUENCE     �   ALTER TABLE public.tb_test_step ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_test_step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    208            �            1259    16446    tb_test_step_result    TABLE     d  CREATE TABLE public.tb_test_step_result (
    id integer NOT NULL,
    test_step_id character varying(255),
    test_cycle_id integer,
    test_module_id character varying(255),
    test_scenario_id character varying(255),
    status_id integer,
    project_id integer,
    start_datetime character varying(150),
    end_datetime character varying(150)
);
 '   DROP TABLE public.tb_test_step_result;
       public         postgres    false            �            1259    16567    tb_test_step_result_id_seq    SEQUENCE     �   ALTER TABLE public.tb_test_step_result ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_test_step_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    209            �            1259    16452    tb_tester_info    TABLE     �   CREATE TABLE public.tb_tester_info (
    tester_id integer NOT NULL,
    firstname character varying(255),
    lastname character varying(255)
);
 "   DROP TABLE public.tb_tester_info;
       public         postgres    false            �            1259    16518    tb_tester_info_tester_id_seq    SEQUENCE     �   ALTER TABLE public.tb_tester_info ALTER COLUMN tester_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_tester_info_tester_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    210            �            1259    16487    tb_type_of_testing    TABLE     x   CREATE TABLE public.tb_type_of_testing (
    testing_id integer NOT NULL,
    type_of_testing character varying(255)
);
 &   DROP TABLE public.tb_type_of_testing;
       public         postgres    false            �            1259    16512 !   tb_type_of_testing_testing_id_seq    SEQUENCE     �   ALTER TABLE public.tb_type_of_testing ALTER COLUMN testing_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tb_type_of_testing_testing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    213            k          0    16505 
   tb_browser 
   TABLE DATA               9   COPY public.tb_browser (browser_id, browser) FROM stdin;
    public       postgres    false    215   .X       X          0    16394 
   tb_project 
   TABLE DATA               R   COPY public.tb_project (proj_id, proj_name, proj_description, client) FROM stdin;
    public       postgres    false    196   tX       Z          0    16402    tb_project_status 
   TABLE DATA               O   COPY public.tb_project_status (proj_status_id, proj_id, status_id) FROM stdin;
    public       postgres    false    198   �X       g          0    16476 	   tb_status 
   TABLE DATA               B   COPY public.tb_status (status_id, status, definition) FROM stdin;
    public       postgres    false    211   �X       \          0    16407    tb_test_collection 
   TABLE DATA               S   COPY public.tb_test_collection ("ID", test_scenario_id, test_scenario) FROM stdin;
    public       postgres    false    200   �Y       ]          0    16413    tb_test_collection_result 
   TABLE DATA               �   COPY public.tb_test_collection_result ("ID", test_scenario_id, test_cycle_id, test_module_id, project_id, status_id, start_datetime, end_datetime, duration) FROM stdin;
    public       postgres    false    201   �Y       _          0    16421    tb_test_cycle 
   TABLE DATA               �   COPY public.tb_test_cycle (test_cycle_id, test_cycle_name, test_cycle_description, test_duration, tester_id, env_id, testing_id, browser_id, browser_version, start_datetime, end_datetime) FROM stdin;
    public       postgres    false    203   [       j          0    16492    tb_test_environment 
   TABLE DATA               G   COPY public.tb_test_environment (env_id, test_environment) FROM stdin;
    public       postgres    false    214   �[       a          0    16429    tb_test_module 
   TABLE DATA               F   COPY public.tb_test_module ("ID", module_id, module_name) FROM stdin;
    public       postgres    false    205   (\       b          0    16435    tb_test_module_result 
   TABLE DATA               �   COPY public.tb_test_module_result ("ID", module_id, test_cycle_id, proj_id, status_id, start_datetime, end_datetime, duration) FROM stdin;
    public       postgres    false    206   �\       d          0    16440    tb_test_step 
   TABLE DATA               [   COPY public.tb_test_step (id, test_step_id, test_step_description, test_value) FROM stdin;
    public       postgres    false    208   �]       e          0    16446    tb_test_step_result 
   TABLE DATA               �   COPY public.tb_test_step_result (id, test_step_id, test_cycle_id, test_module_id, test_scenario_id, status_id, project_id, start_datetime, end_datetime) FROM stdin;
    public       postgres    false    209   �^       f          0    16452    tb_tester_info 
   TABLE DATA               H   COPY public.tb_tester_info (tester_id, firstname, lastname) FROM stdin;
    public       postgres    false    210   �a       i          0    16487    tb_type_of_testing 
   TABLE DATA               I   COPY public.tb_type_of_testing (testing_id, type_of_testing) FROM stdin;
    public       postgres    false    213   �a       z           0    0    tb_browser_browser_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.tb_browser_browser_id_seq', 5, true);
            public       postgres    false    216            {           0    0    tb_project_ID_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public."tb_project_ID_seq"', 73, true);
            public       postgres    false    197            |           0    0 $   tb_project_status_proj_status_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.tb_project_status_proj_status_id_seq', 75, true);
            public       postgres    false    199            }           0    0    tb_status_status_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.tb_status_status_id_seq', 7, true);
            public       postgres    false    212            ~           0    0    tb_test_collection_ID_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."tb_test_collection_ID_seq"', 169, true);
            public       postgres    false    202                       0    0    tb_test_collection_ID_seq1    SEQUENCE SET     L   SELECT pg_catalog.setval('public."tb_test_collection_ID_seq1"', 141, true);
            public       postgres    false    219            �           0    0    tb_test_cycle_test_cycle_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.tb_test_cycle_test_cycle_id_seq', 161, true);
            public       postgres    false    204            �           0    0    tb_test_environment_env_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.tb_test_environment_env_id_seq', 4, true);
            public       postgres    false    221            �           0    0    tb_test_module_ID_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."tb_test_module_ID_seq"', 131, true);
            public       postgres    false    207            �           0    0    tb_test_module_ID_seq1    SEQUENCE SET     H   SELECT pg_catalog.setval('public."tb_test_module_ID_seq1"', 257, true);
            public       postgres    false    218            �           0    0    tb_test_step_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tb_test_step_id_seq', 367, true);
            public       postgres    false    222            �           0    0    tb_test_step_result_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.tb_test_step_result_id_seq', 1331, true);
            public       postgres    false    223            �           0    0    tb_tester_info_tester_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.tb_tester_info_tester_id_seq', 70, true);
            public       postgres    false    220            �           0    0 !   tb_type_of_testing_testing_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.tb_type_of_testing_testing_id_seq', 3, true);
            public       postgres    false    217            �
           2606    16509    tb_browser tb_browser_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.tb_browser
    ADD CONSTRAINT tb_browser_pkey PRIMARY KEY (browser_id);
 D   ALTER TABLE ONLY public.tb_browser DROP CONSTRAINT tb_browser_pkey;
       public         postgres    false    215            �
           2606    16459    tb_project tb_project_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.tb_project
    ADD CONSTRAINT tb_project_pkey PRIMARY KEY (proj_id);
 D   ALTER TABLE ONLY public.tb_project DROP CONSTRAINT tb_project_pkey;
       public         postgres    false    196            �
           2606    16461 (   tb_project_status tb_project_status_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.tb_project_status
    ADD CONSTRAINT tb_project_status_pkey PRIMARY KEY (proj_status_id);
 R   ALTER TABLE ONLY public.tb_project_status DROP CONSTRAINT tb_project_status_pkey;
       public         postgres    false    198            �
           2606    16480    tb_status tb_status_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.tb_status
    ADD CONSTRAINT tb_status_pkey PRIMARY KEY (status_id);
 B   ALTER TABLE ONLY public.tb_status DROP CONSTRAINT tb_status_pkey;
       public         postgres    false    211            �
           2606    16463 &   tb_test_module tb_test_collection_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.tb_test_module
    ADD CONSTRAINT tb_test_collection_pkey PRIMARY KEY ("ID");
 P   ALTER TABLE ONLY public.tb_test_module DROP CONSTRAINT tb_test_collection_pkey;
       public         postgres    false    205            �
           2606    16465 +   tb_test_collection tb_test_collection_pkey1 
   CONSTRAINT     k   ALTER TABLE ONLY public.tb_test_collection
    ADD CONSTRAINT tb_test_collection_pkey1 PRIMARY KEY ("ID");
 U   ALTER TABLE ONLY public.tb_test_collection DROP CONSTRAINT tb_test_collection_pkey1;
       public         postgres    false    200            �
           2606    16475     tb_test_cycle tb_test_cycle_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.tb_test_cycle
    ADD CONSTRAINT tb_test_cycle_pkey PRIMARY KEY (test_cycle_id);
 J   ALTER TABLE ONLY public.tb_test_cycle DROP CONSTRAINT tb_test_cycle_pkey;
       public         postgres    false    203            �
           2606    16496 ,   tb_test_environment tb_test_environment_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.tb_test_environment
    ADD CONSTRAINT tb_test_environment_pkey PRIMARY KEY (env_id);
 V   ALTER TABLE ONLY public.tb_test_environment DROP CONSTRAINT tb_test_environment_pkey;
       public         postgres    false    214            �
           2606    16467 0   tb_test_module_result tb_test_module_result_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.tb_test_module_result
    ADD CONSTRAINT tb_test_module_result_pkey PRIMARY KEY ("ID");
 Z   ALTER TABLE ONLY public.tb_test_module_result DROP CONSTRAINT tb_test_module_result_pkey;
       public         postgres    false    206            �
           2606    16469    tb_test_step tb_test_step_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tb_test_step
    ADD CONSTRAINT tb_test_step_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tb_test_step DROP CONSTRAINT tb_test_step_pkey;
       public         postgres    false    208            �
           2606    16471 ,   tb_test_step_result tb_test_step_result_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.tb_test_step_result
    ADD CONSTRAINT tb_test_step_result_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.tb_test_step_result DROP CONSTRAINT tb_test_step_result_pkey;
       public         postgres    false    209            �
           2606    16473 "   tb_tester_info tb_tester_info_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.tb_tester_info
    ADD CONSTRAINT tb_tester_info_pkey PRIMARY KEY (tester_id);
 L   ALTER TABLE ONLY public.tb_tester_info DROP CONSTRAINT tb_tester_info_pkey;
       public         postgres    false    210            �
           2606    16491 *   tb_type_of_testing tb_type_of_testing_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.tb_type_of_testing
    ADD CONSTRAINT tb_type_of_testing_pkey PRIMARY KEY (testing_id);
 T   ALTER TABLE ONLY public.tb_type_of_testing DROP CONSTRAINT tb_type_of_testing_pkey;
       public         postgres    false    213            k   6   x�3�L�(��M�2�L�,JM˯�2��L�2�,NLK,��2��/H-J����� ?��      X   ;   x�37�(��JM.Q�K-NNLK�t��/�H-R(-HI,IU�,V�MLIU��s��qqq 9�      Z      x�37�47�4����� _�      g   �   x���K�0@��)z4���+#	.�4e���i�r)������{�Y��D���JU�a�ǥ#R�X�^4߲���y�N_�Bz������k�Z��*Ė�+��ѐm�F$#��Q�X���)Lۏ�1����'���=���Bh�9�LVG&��¨�	�ɡUW�u����ǂs��.��      \   A   x�34��q�7�K��LQ��O���241 	s�敤)x�V�AB&��y�%@A��=... ~y      ]   �   x����NC!F��<@�~�m׮첉Qs]�1����M��\H�	�;�>������|�N�z�����t1�v�����fJr棂ϋy0p�:��L=�� _ ��Oo}iPFh1b��� ���!`�䛤p�;)�+I�J�ݒEK�-��+H�0�J`-AM�<��_R�xv�.��6�7�J��M�ndP�� ��v��ǖl� gv@���-Q��n�c��r�hY"��G��wJ�Q      _   �   x���=�@@�ܯ��Jr����"b����E�I+���"���L�½G؍!�/����p�u��L5�G��bȊ��v��"�[��MYa�v�PQ� i�>�mrH,h`���Դ_.Cݔ��W�f�X��oK�8�E�N~)�J:���G����ɋ3b����Z� �d.iK�e)��v-�K:��j5TJ] ]���      j   D   x�3�tI-S�/R��ON��2�tTp�+�,���M�+�2�.IL��K�2�(�O)M.�������� 0%      a   M   x�325��7���O��S�U0�225	q�yf �)\�P�$d�韗��X�����������Z����� {5�      b   �   x���;N�0Ekg^ ��s���HP$(f4ؿp�~��L�8�=9��8���7t����=/����=�'��q.BE��Bt�u�����B�Y>'ĸ����;>��SiZiPJ;��b\ ����y��L��y��%�Ť�6Z.jB��#5i���2o��w���f���W��逸�C�Ʌ��d�Ʀ���o�s�����&�޲�̱G��I��4#�R�����o��5�F�Rӎ�eͨ����4M�;�      d   �   x�U��j�  �>��I�$�sڲ�P(%��V(nb�D���o_�b/:���Ȫ&o��E�ɋ��^�f��]��M�9�<���3#����!�A>�ISYm�� ��g�A{�&M��g4��j��ge/�n�;,�KA�B�;ߑ���w\�ML�cZIN�i/���	H<ٓ��.P������ �u�&":G��[]�o�>q��K��a�}�x�z��H�N����ƻ/3j�pS�<����d.�e�+��GF)���G      e     x���͊�0���y�R4�o_{�is,,r,����ˑ��Kx.{�������,�������������Ƿ�L�N����~>�܍�rG�av8�t<�p\~aeq�� ̐�\X;��	�Ma�+Έ��z=���c�;cma�2�
��X/͈����lXY0zl �l�m��ǜ�e�`pˢ4#6�&xb�+5!/l0�	����f�k���E�Mȝ��
�k˽�݄��X�n����-����bC���`��q��glja�ds���{Mc/�ux��¨��9!b�;+-/]*�(�Ѳ��H�r��:,3zo�c���J��ބ���^fa}d�-o�#+ld�{�]߄Ė��ބ�f��:l2ly��tM����=<߾Xd˫�Z����˥Ҍ�,���td8L��.5y�؝m>�9:�'_�w�uҌη)��a���j�t�M�.����\�W���gS�w�g,��Ue�.����.�*���j����ׯ$#lf��k�A����aFϑ5���36�&��(͈ͬ	��%��ք>V��	J,�&\g��5��z[Xǚ��zք�'6�&��B����Dք>V�maSkt��lp�]����[�Y�M��5a��M�kF,�&(�H,�&���ygmk�&�ZXqF�oM�dCk�KM�ؚ0��6�M�	J,�rk� ���mFM@Úp�����S�6K�kB�{��f�XdM�cw�鼳�5�2k�`�X�X+����5�:Ka�����
a,ldMPbk�u��5��z��ְ&\g)�X�Xi�n?��n�����      f      x�37�����t--.I,,������� Ka!      i   =   x�3����NUI-.��K�2�JM/J-.��σq��)8&'��$�%#���qqq 8     