from Utility.excel_mgmt import ExcelExec
import psycopg2

__author__ = "Yom Eustaquio"
__copyright__ = "2019, Test Automation Demo"
__version__ = "0.22.8.3"
__maintainer__ = "Yom Eustaquio"
__email__ = "yom.eustaquio@gmail.com"
__status__ = "Dev"

class DbExec:

    def connect(self):
        try:
            conn = psycopg2.connect(user="postgres", password="P@ssw0rd123!", host="127.0.0.1", port="5432", database="DBTestAutomation")
            cur = conn.cursor()
        except (Exception, psycopg2.DatabaseError) as error:
            print("Connection error from the PostgreSQL Database: ", error)
 
        return conn, cur

    def validate_tester(self, testerInfo):
        testerName = testerInfo.split()
        testerLen = len(testerName)

        if testerLen > 1:
            tester = self.get_tester_info(testerName)
            if tester is None:
                self.insert_tester(testerName)
            elif tester[1] == testerName[0] and tester[2] == testerName[len(testerName) - 1]:
                return tester[0] #tester ID
        else:
            testerName.append('Tester')
            tester = self.get_tester_info(testerName)
            if tester is None:
                self.insert_tester(testerName)
            elif tester[1] == testerName[0] and tester[2] == testerName[len(testerName) - 1]:
                return tester[0] #tester ID

    def insert_tester(self, testerName):
        conn, cur = self.connect()

        cur.execute("INSERT INTO tb_tester_info (firstname, lastname) VALUES ('{}', '{}')".format(testerName[0], testerName[len(testerName)-1]))

        conn.commit()

    def get_tester_info(self, testerName):
        conn, cur = self.connect()

        try:
            cur.execute("SELECT * FROM tb_tester_info WHERE firstname = '{}' AND lastname = '{}'".format(testerName[0], testerName[len(testerName)-1]))
            fullname = cur.fetchone()
            return fullname
        except Exception as e:
            print("Unable to fetch tester info: ", e)

    def validate_test_cycle(self, test_cycle_name, test_def, start_datetime, end_datetime, duration, testerInfo, envInfo, typeOftesting, browserInfo, browserVersion):
        cycle = self.get_test_cycle_name(test_cycle_name)
        tester_id = self.validate_tester(testerInfo)
        browser_id = self.get_browser(browserInfo)
        environment_id = self.get_environment(envInfo)
        testing_id = self.type_of_testing(typeOftesting)

        if cycle is None:
            self.init_test_cycle(test_cycle_name, test_def, tester_id, environment_id, testing_id, browser_id, browserVersion, start_datetime)
        elif cycle[1] == test_cycle_name:
            self.update_test_cycle(test_cycle_name, end_datetime, duration)

    def init_test_cycle(self, test_cycle_name, test_def, tester_id, environment_id, testing_id, browser_id, browserVersion, start_datetime):

        conn, cur = self.connect()
        cur.execute("INSERT INTO tb_test_cycle (test_cycle_name, test_cycle_description, tester_id, env_id, "
                    "testing_id, browser_id, browser_version, start_datetime) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
            test_cycle_name,
            test_def,
            tester_id,
            environment_id[0],
            testing_id[0],
            browser_id[0],
            browserVersion,
            start_datetime
        ))

        conn.commit()

    def update_test_cycle(self, test_cycle_name, end_datetime, duration,):
        conn, cur = self.connect()
        cur.execute("UPDATE tb_test_cycle SET end_datetime='{}', test_duration = '{}' WHERE test_cycle_name = '{}'".format(end_datetime, duration, test_cycle_name))
        conn.commit()

    def get_latest_cycle(self):
        conn, cur = self.connect()
        cur.execute("SELECT MAX(test_cycle_id) FROM tb_test_cycle")
        cycle_id = cur.fetchone()

        return cycle_id

    def get_test_cycle_id(self, test_cycle_name):
        conn, cur = self.connect()
        cur.execute("SELECT test_cycle_id FROM tb_test_cycle WHERE test_cycle_name = '{}'".format(test_cycle_name))
        cycle_id = cur.fetchone()

        return cycle_id

    def insert_test_cycle(self, test_cycle_name, test_def, start_datetime, end_datetime, duration, testerInfo, envInfo, typeOftesting, browserInfo, browserVersion):
        conn, cur = self.connect()

        cur.execute("INSERT INTO tb_test_cycle (test_cycle_name, test_cycle_description, tester_id, "
                    "env_id, testing_id, browser_id, browser_version, start_datetime, end_datetime, test_duration)"
                    "VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
            test_cycle_name,
            test_def,
            testerInfo,
            envInfo,
            typeOftesting,
            browserInfo,
            browserVersion,
            start_datetime,
            end_datetime,
            duration,
        ))

        conn.commit()

    def get_test_cycle_name(self, test_cycle_name):
        conn, cur = self.connect()
        cur.execute("SELECT * FROM tb_test_cycle WHERE test_cycle_name = '{}'".format(test_cycle_name))
        cycle_data = cur.fetchone()

        return cycle_data

    def get_browser(self, browserInfo):
        conn, cur = self.connect()
        cur.execute("SELECT * FROM tb_browser WHERE browser = '{}'".format(browserInfo))
        browser = cur.fetchone()

        return browser

    def get_environment(self, envInfo):
        conn, cur = self.connect()
        cur.execute("SELECT * FROM tb_test_environment WHERE test_environment = '{}'".format(envInfo))
        environment = cur.fetchone()

        return environment

    def type_of_testing(self, typeOftesting):
        conn, cur = self.connect()
        cur.execute("SELECT * FROM tb_type_of_testing WHERE type_of_testing = '{}'".format(typeOftesting))
        testing = cur.fetchone()

        return testing

    def validate_project(self, projectInfo):
        try:
            pr_data = self.get_project_info(projectInfo)

            if pr_data is None:
                self.insert_new_project(projectInfo)
                self.insert_project_status(projectInfo)
            elif str(pr_data[1]) == str(projectInfo[0]) and str(pr_data[3]) == str(projectInfo[1]):
                if pr_data[2] != projectInfo[2]:
                    self.update_project_def(projectInfo)
                self.update_project_status(projectInfo)
        except Exception as e:
            print("Unable to validate project: ", e)

    def get_project_info(self, projectInfo):
        conn, cur = self.connect()
        try:
            cur.execute("SELECT * FROM tb_project WHERE proj_name = '{}' AND client = '{}'".format(projectInfo[0], projectInfo[1]))
            pr_data = cur.fetchone()
            return pr_data
        except Exception as e:
            print("Unable to fetch project info: ", e)

    def insert_new_project(self, projectInfo):
        conn, cur = self.connect() 
        try:
            cur.execute("INSERT INTO tb_project (proj_name, client, proj_description) VALUES ('{}', '{}', '{}')".format(projectInfo[0], projectInfo[1], projectInfo[2]))
            print("New project is added: {}".format(projectInfo[0]))
        except Exception as e:
            print("Unable to insert new project: ", e)
        conn.commit()

    def get_project_id(self, projectInfo):
        conn, cur = self.connect()
        try:
            cur.execute("SELECT proj_id FROM tb_project WHERE proj_name = '{}' AND client = '{}'".format(projectInfo[0], projectInfo[1]))
            pr_data = cur.fetchone()
            return pr_data[0]
        except Exception as e:
            print("Unable to fetch project id: ", e)

    def get_proj_status_id(self, status):
        conn, cur = self.connect()
        try:
            for i in range(0, len(status)):
                if status[i] == 'On-Going' or status[i] == 'Completed' or status[i] == 'Closed' or status[i] == 'Demo':
                    stat = status[i]
                    cur.execute("SELECT status_id FROM tb_status WHERE status = '{}'".format(stat))
                    stat_id = cur.fetchone()
                    return stat_id[0]
        except Exception as e:
            print("Unable to fetch project status id: ", e)

    def insert_project_status(self, projectInfo):
        status_id = self.get_proj_status_id(projectInfo)
        get_project_id = self.get_project_id(projectInfo)

        conn, cur = self.connect()
        try:
            cur.execute("INSERT INTO tb_project_status (proj_id, status_id) VALUES ('{}', '{}')".format(get_project_id, status_id))
        except Exception as e:
            print("Unable to insert project status: ", e)
        conn.commit()

    def update_project_def(self, projectInfo):
        get_project_id = self.get_project_id(projectInfo)

        conn, cur = self.connect()
        try:
            cur.execute("UPDATE tb_project SET proj_description = '{}' WHERE proj_id= '{}'".format(projectInfo[2], get_project_id))
        except Exception as e:
            print("Unable to update project definition: ", e)
        conn.commit()

    def update_project_status(self, projectInfo):
        status_id = self.get_proj_status_id(projectInfo)
        get_project_id = self.get_project_id(projectInfo)

        conn, cur = self.connect()
        try:
            cur.execute("SELECT status_id FROM tb_project_status WHERE proj_id = '{}'".format(get_project_id))
            current_stat = cur.fetchone()

            if current_stat is None:
                self.insert_project_status(projectInfo)
            elif status_id != current_stat[0]:
                cur.execute("UPDATE tb_project_status SET status_id = {} WHERE proj_id= {}".format(status_id, get_project_id))

        except Exception as e:
            print("Unable to update project status: ", e)
        conn.commit()

    def validate_module(self, project_info, excel_tm_id, excel_module_name, run_mode, test_cycle, start, end, duration):
        proj_id = self.get_project_id(project_info)
        cycle_id = self.get_test_cycle_id(test_cycle)[0]
        mod_info = self.get_mod_info(excel_tm_id, proj_id)

        if run_mode == "YES":
            run_mode = "Passed"
            mod_stat = self.get_status_id(run_mode)
        else:
            mod_stat = self.get_status_id(run_mode)

        if mod_info is None:
            self.insert_mod_info(excel_tm_id, excel_module_name)
            self.insert_mod_result(excel_tm_id, proj_id, cycle_id, mod_stat, start, end, duration)

        mod_info_cyc = self.get_test_mod_cycle(proj_id, excel_tm_id, cycle_id)

        if mod_info_cyc is None:
            self.insert_mod_result(excel_tm_id, proj_id, cycle_id, mod_stat, start, end, duration)
        else:
            self.update_test_mod_result(excel_tm_id, cycle_id, end, duration, mod_stat)

    def update_test_mod_result(self, excel_tm_id, cycle_id, end, duration, stat_id):
        conn, cur = self.connect()
        cur.execute("UPDATE tb_test_module_result SET end_datetime = '{}', duration='{}', status_id={}" 
                    "WHERE test_cycle_id='{}' "
                    "AND module_id='{}'".format(end, duration, stat_id, cycle_id, excel_tm_id))
        conn.commit()

    def insert_mod_info(self, excel_tm_id, excel_module_name):
        conn, cur = self.connect()
        cur.execute("INSERT INTO tb_test_module (module_id, module_name) VALUES ('{}', '{}')".format(excel_tm_id, excel_module_name))
        conn.commit()

    def insert_mod_result(self, excel_tm_id, project_id, test_cycle_id, status_id, start, end, duration):
        conn, cur = self.connect()

        cur.execute("INSERT INTO tb_test_module_result "
                    "(module_id, test_cycle_id, proj_id, status_id, start_datetime, end_datetime, duration) "
                    "VALUES ('{}', {}, {}, {}, '{}', '{}', '{}')".format(excel_tm_id, test_cycle_id, project_id, status_id, start, end, duration))
        conn.commit()


    def get_mod_info(self, excel_tm_id, projectinfo):
        conn, cur = self.connect()
        cur.execute(""" 
                        SELECT * FROM tb_test_module AS module
                        INNER JOIN tb_test_module_result AS results ON
                        module.module_id = results.module_id
                        WHERE results.proj_id = {} 
                        AND results.module_id = '{}'
                    """.format(projectinfo, excel_tm_id))
        result = cur.fetchone()

        return result

    def get_test_mod_cycle(self, projectinfo, module_info, test_cycle):
        conn, cur = self.connect()
        cur.execute(""" 
                SELECT * FROM tb_test_module AS module
                INNER JOIN tb_test_module_result AS results ON
                module.module_id = results.module_id
                WHERE results.proj_id = {}
                AND results.module_id = '{}'
                AND results.test_cycle_id = '{}'
            """.format(projectinfo, module_info, test_cycle))
        result = cur.fetchone()

        return result


    def validate_test_scen(self, projectInfo, test_cycle, excel_module_id, excel_tc_id, excel_tc_name, run_mode, start, end, duration):
        project_id = self.get_project_id(projectInfo)
        cycle_id = self.get_test_cycle_id(test_cycle)[0]

        if run_mode == "YES":
            run_mode = "Passed"
            mod_stat = self.get_status_id(run_mode)
        else:
            mod_stat = self.get_status_id(run_mode)

        result = self.get_test_scen_result(project_id, excel_module_id, excel_tc_id)

        if result is None:
            self.insert_test_scen(excel_tc_id, excel_tc_name)
            self.insert_test_scen_stat(project_id, cycle_id, excel_module_id, excel_tc_id, mod_stat, start, end,
                                       duration)

        result_cycle = self.get_test_scen_cycle(project_id, excel_module_id, excel_tc_id, cycle_id)

        if result_cycle is None:
            self.insert_test_scen_stat(project_id, cycle_id, excel_module_id, excel_tc_id, mod_stat, start, end,
                                       duration)
        else:
            self.update_test_scen_result(excel_tc_id, cycle_id, end, duration, mod_stat)

    def update_test_scen_result(self, excel_tc_id, cycle_id, end, duration, stat_id):
        conn, cur = self.connect()
        cur.execute("UPDATE tb_test_collection_result SET end_datetime = '{}', duration='{}', status_id={}" 
                    "WHERE test_cycle_id='{}' "
                    "AND test_scenario_id='{}'".format(end, duration, stat_id, cycle_id, excel_tc_id))
        conn.commit()

    def insert_test_scen(self, excel_tc_id, excel_tc_name):
        conn, cur = self.connect()
        cur.execute("INSERT INTO tb_test_collection (test_scenario_id, test_scenario) "
                    "VALUES ('{}', '{}')".format(excel_tc_id, excel_tc_name))
        conn.commit()

    def insert_test_scen_stat(self, projectInfo, test_cycle, excel_module_id, excel_tc_id, stat_id, start, end, duration):
        conn, cur = self.connect()

        cur.execute("INSERT INTO tb_test_collection_result "
                    "(project_id, test_cycle_id, test_module_id, test_scenario_id, status_id, start_datetime, end_datetime, duration) "
                    "VALUES ({}, '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(projectInfo, test_cycle, excel_module_id, excel_tc_id, stat_id, start, end, duration))
        conn.commit()

    def get_test_scen_cycle(self, projectinfo, module_info, test_scenario_info, test_cycle):
        conn, cur = self.connect()
        cur.execute(""" 
                SELECT scenario.test_scenario_id, results.test_cycle_id
                FROM tb_test_collection AS scenario
                INNER JOIN tb_test_collection_result AS results ON
                scenario.test_scenario_id = results.test_scenario_id
                WHERE results.project_id = {} 
                AND results.test_module_id = '{}'
                AND results.test_scenario_id = '{}'
                AND results.test_cycle_id = '{}'
            """.format(projectinfo, module_info, test_scenario_info, test_cycle))
        result = cur.fetchone()

        return result

    def get_test_scen_result(self, projectinfo, module_info, test_scenario_info):
        conn, cur = self.connect()
        cur.execute(""" 
                SELECT scenario.test_scenario_id, results.test_cycle_id
                FROM tb_test_collection AS scenario
                INNER JOIN tb_test_collection_result AS results ON
                scenario.test_scenario_id = results.test_scenario_id
                WHERE results.project_id = {} 
                AND results.test_module_id = '{}'
                AND results.test_scenario_id = '{}'
            """.format(projectinfo, module_info, test_scenario_info))
        result = cur.fetchone()

        return result

    def validate_test_step(self, step_id, test_step_description, test_value, projectinfo, test_cycle, module_info, test_scenario_info,start_datetime, end_datetime, status):

        project_id = self.get_project_id(projectinfo)
        stat_id = self.get_status_id(status)
        cycle_id = self.get_test_cycle_id(test_cycle)

        result = self.get_test_step(project_id, step_id)

        if result is None:
            self.insert_test_step(step_id, test_step_description, test_value)
            self.insert_test_step_result(project_id, cycle_id[0], module_info, test_scenario_info, step_id, stat_id,
                                         start_datetime, end_datetime)
        else:
            self.insert_test_step_result(project_id, cycle_id[0], module_info, test_scenario_info, step_id, stat_id,
                                         start_datetime, end_datetime)

    def insert_test_step(self, step_id, test_step_description, test_value):
        conn, cur = self.connect()
        cur.execute("INSERT INTO tb_test_step (test_step_id, test_step_description, test_value) VALUES ('{}','{}','{}')".format(step_id, test_step_description, test_value))
        conn.commit()

    def insert_test_step_result(self, project_id, test_cycle, module_info, test_scenario_info, step_id, status_id, start_datetime, end_datetime):
        conn, cur = self.connect()
        cur.execute("INSERT INTO tb_test_step_result "
                    "(project_id, test_cycle_id, test_module_id, test_scenario_id, test_step_id, status_id, start_datetime, end_datetime)"
                    " VALUES ({}, '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(project_id, test_cycle, module_info, test_scenario_info, step_id, status_id, start_datetime, end_datetime))
        conn.commit()

    def get_test_step(self, projectinfo, step_id):
        conn, cur = self.connect()
        cur.execute(""" 
            SELECT step.test_step_id, results.project_id 
            FROM tb_test_step as step
            INNER JOIN tb_test_step_result AS results ON step.test_step_id = results.test_step_id
            WHERE results.project_id = {}
            AND step.test_step_id = '{}'
        """.format(projectinfo, step_id))
        result = cur.fetchone()

        return result

    def get_status_id(self, status):
        stat = status.capitalize()
        conn, cur = self.connect()
        cur.execute("SELECT status_id FROM tb_status WHERE status = '{}'".format(stat))
        id = cur.fetchone()

        return id[0]





