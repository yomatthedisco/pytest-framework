from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from Utility.constant import Constant
import time
from datetime import datetime

__author__ = "Yom Eustaquio"
__copyright__ = "2019, Test Automation Demo"
__version__ = "0.2.8.7"
__maintainer__ = "Yom Eustaquio"
__email__ = "yom.eustaquio@gmail.com"
__status__ = "Dev"


class SeleniumDriver:

    def __init__(self, driver):
        self.driver = driver
        self.constant = Constant()

    def navigate(self, datavalue):
        try:
            self.driver.get(datavalue)
            message = "Website is navigated {}".format(datavalue)
            #self.capture_screen(message, "PASSED")
            print(message)
            return True
        except:
            message = "Unable to navigate the website {}".format(datavalue)
            #self.capture_screen(message, "FAILED")
            print(message)
            return False

    def refreshBrowser(self, datavalue):
        try:
            self.driver.refresh()
            message = "{} is refreshed".format(datavalue)
            #self.capture_screen(message, "PASSED")
            print(message)
            return True
        except:
            message = "Unable to refresh {}".format(datavalue)
            #self.capture_screen(message, "FAILED")
            print(message)
            return False

    def getElement(self, data, locator):
        try:
            time.sleep(0.4)
            element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.XPATH, locator)))
            return element
        except:
            message = "Unable to find element for {} - {}".format(data, locator)
            #self.capture_screen(message, "FAILED")
            print(message)

    def sendKeys(self, data, locator):
        try:

            self.getElement(data, locator).clear()
            self.getElement(data, locator).send_keys(data)
            message = "{} is entered".format(data)
            print(message)
            #self.capture_screen(message, "PASSED")
            return True
        except:
            message = "Unable to enter {} ".format(data)
            print(message)
            #self.capture_screen(message, "FAILED")
            return False

    def upload(self, data, locator):
        try:

            self.getElement(data, locator).send_keys(data)
            message = "{} is entered".format(data)
            print(message)
            # self.capture_screen(message, "PASSED")
            return True
        except:
            message = "Unable to upload {} ".format(data)
            print(message)
            #self.capture_screen(message, "FAILED")
            return False

    def elementClick(self, data, locator):
        try:

            self.getElement(data, locator).click()
            message = "{} is clicked".format(data)
            print(message)
            #self.capture_screen(message, "PASSED")
            return True
        except:
            message = "Unable to click {}".format(data)
            print(message)
            #self.capture_screen(message, "FAILED")
            return False

    def specialKey(self, data, locator):
        try:
            if data.upper() == "ENTER":

                self.getElement(data, locator).send_keys(Keys.ENTER)
                message = "{} is entered".format(locator)
                print(message)
                #self.capture_screen(message, "PASSED")
                return True
        except:
            message = "Unable to click {}".format(data)
            print(message)
            #self.capture_screen(message, "FAILED")
            return False

    def webScroll(self, direction):

        try:
            if direction == 'up':
                self.driver.execute_script("window.scrollBy(0,-1000);")
                return True
            elif direction == 'down':
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                return True
            elif direction == 'right':
                self.driver.execute_script("window.scrollBy(1000,0);")
                return True
            else:
                self.driver.execute_script("window.scrollBy(-1000,0);")
                return True
        except:
            print("Scrolling failed")
            return False

    def capture_screen(self, filename, status):
        now = datetime.now()
        try:
            passed_location = self.constant.Path_Screenshot_Passed_Step
            failed_location = self.constant.Path_Screenshot_Failed_Step
            my_time = now.strftime("%m-%d-%Y %H-%M-%S")

            if status == "PASSED":
                destination = passed_location + my_time + " " + filename + ".png"
                self.driver.save_screenshot(destination)
                #print("Screen captured {}".format(my_time))
                return True
            elif status == "FAILED":
                destination = failed_location + my_time + " " + filename + ".png"
                self.driver.save_screenshot(destination)
                #print("Screen captured {}".format(my_time))
                return True
            else:
                #print("Unable to capture screen, test no result.")
                return False

        except NotADirectoryError:
            #print("Unable to capture screen.")
            return False