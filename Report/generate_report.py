from jinja2 import Environment, FileSystemLoader
import os


class GenerateReport:

    def __init__(self):
        
        self.root = os.path.dirname(os.path.abspath(__file__))
        templates_dir = os.path.join(self.root, 'templates')
        env = Environment(loader = FileSystemLoader(templates_dir) )
        self.template = env.get_template('report.html')
    
    def generateReport(self):
        filename = os.path.join(self.root, 'generated', 'generated-new.html')
        with open(filename, 'w') as fh:
            fh.write(self.template.render(
                h1 = "Hello Jinja2",
                test1 = True,
                test2 = False,
                names    = ["test", "test", "scenario"],
            ))
    
    def graphFormula(self, dash_array_x):
        circumference = 100
        stroke_dashoffset = 25
        dash_array_y = circumference - dash_array_x
        dashoffset = 25
        
        result = (circumference - dash_array_x) + " "
        
        passedColor = "#00af00"
        failedColor = "#d90000"
        skippedColor = "#FFDF00"
        
    
    def getPassedResult(self, passedScenario):
        pass
    
    def getFailedResult():
        pass
    
    def getSkippedResult():
        pass
    
    def getImageFailedResult():
        pass
    
    def getImagePassedResult():
        pass
    
    def htmlContent():
        pass
    
    def titleReport():
        pass
    
    def writeReport():
        pass
    
    def closeReport():
        pass
    
    def openReport():
        pass
    
report = GenerateReport()

report.generateReport()