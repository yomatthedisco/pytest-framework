import pytest
from Utility.excel_mgmt import ExcelExec
from Utility.constant import Constant
from Utility.module_mapping import DriverMapping
from DbConnection.sql_exec import DbExec
from Utility.time_stamp import TimeStamp
import time

__author__ = "Yom Eustaquio"
__copyright__ = "2019, Test Automation Demo"
__version__ = "0.3.8.3"
__maintainer__ = "Yom Eustaquio"
__email__ = "yom.eustaquio@gmail.com"
__status__ = "Dev"


@pytest.mark.usefixtures("setup")
class TestMainExec:

    @pytest.fixture(autouse=True)
    def initialSetup(self, setup):
        self.constant = Constant()
        self.excel = ExcelExec()
        self.driver_method = DriverMapping(self.driver)
        self.sql_exe = DbExec()
        self.stamp = TimeStamp()

    @pytest.mark.run(order=1)
    def test_main(self):
        self.execute_test_main()

    def execute_test_main(self):
        start_time = time.time()
        start = self.stamp.test_start()
        end = ""
        cycle_duration = ""

        excelFileTestCase = self.excel.setExcelFile(self.constant.Path_Excel_File_TestCase)
        test_module_max_row = self.excel.getRowCount(self.constant.SheetName_testModule) + 1
        test_collection_max_row = self.excel.getRowCount(self.constant.SheetName_testCollection) + 1

        self.sql_exe.validate_project(self.excel.getProjectInfo())
        self.sql_exe.validate_tester(self.excel.getTesterName())

        self.sql_exe.validate_test_cycle(
            self.excel.getTestCycleKey() + " " + str(start),
            self.excel.getTestCycleDef(),
            start,
            end,
            cycle_duration,
            self.excel.getTesterName(),
            self.excel.getEnvironment(),
            self.excel.getTypeOfTesting(),
            "chrome",
            "Browser Version"
        )

        for fromTestModuleRow in range(2, test_module_max_row):

            passed_scenario = 0
            failed_scenario = 0

            passedStep = 0
            failedStep = 0

            countTestModule = 0
            countTestCollection = 0

            testModuleIdVal_1 = self.excel.getCellData(self.constant.SheetName_testModule, fromTestModuleRow,
                                                       self.constant.TestModuleIdCol_1)
            testModuleNameVal_1 = self.excel.getCellData(self.constant.SheetName_testModule, fromTestModuleRow,
                                                         self.constant.TestModuleNameCol_1)
            runModeVal_1 = self.excel.getCellData(self.constant.SheetName_testModule, fromTestModuleRow,
                                                  self.constant.RunModeCol_1)

            if str(runModeVal_1).upper() == "SKIP":
                start_skipped_module = "n/a"
                end_skipped_module = "n/a"
                duration_skipped_module = "n/a"

                self.sql_exe.validate_module(
                   self.excel.getProjectInfo(),
                   testModuleIdVal_1,
                   testModuleNameVal_1,
                   "Skipped",
                   self.excel.getTestCycleKey() + " " + str(start),
                   start_skipped_module,
                   end_skipped_module,
                   duration_skipped_module
                )
                continue

            if str(runModeVal_1).upper() == "YES":
                start_time_mod = time.time()
                start_module = self.stamp.test_start()

                end_module = "n/a"
                mod_duration = "n/a"

                self.sql_exe.validate_module(
                    self.excel.getProjectInfo(),
                    testModuleIdVal_1,
                    testModuleNameVal_1,
                    str(runModeVal_1).upper(),
                    self.excel.getTestCycleKey() + " " + str(start),
                    start_module,
                    end_module,
                    mod_duration
                )

                test_module_max_row = self.excel.getRowCount(testModuleNameVal_1)
                if test_module_max_row == None or test_module_max_row == False:
                    continue

                countTestModule += 1

                for fromTestCollectionRow in range(2, test_collection_max_row):

                    testModuleIdVal_2 = self.excel.getCellData(self.constant.SheetName_testCollection,
                                                               fromTestCollectionRow, self.constant.TestModuleIdCol_2)



                    if testModuleIdVal_1 == testModuleIdVal_2:

                        testModuleNameVal_2 = self.excel.getCellData(self.constant.SheetName_testCollection,
                                                                     fromTestCollectionRow,
                                                                     self.constant.TestModuleNameCol_2)
                        testCaseIdVal_1 = self.excel.getCellData(self.constant.SheetName_testCollection,
                                                                 fromTestCollectionRow, self.constant.TestCaseIdCol_1)
                        testCaseDescriptionVal = self.excel.getCellData(self.constant.SheetName_testCollection,
                                                                        fromTestCollectionRow,
                                                                        self.constant.TestCaseDescription)
                        runModeVal_2 = self.excel.getCellData(self.constant.SheetName_testCollection,
                                                              fromTestCollectionRow, self.constant.RunModeCol_2)

                        if str(runModeVal_2).upper() == "SKIP":

                            start_skipped_scenario = "n/a"
                            end_skipped_scenario = "n/a"
                            duration_skipped_scenario = "n/a"

                            self.sql_exe.validate_test_scen(
                                self.excel.getProjectInfo(),
                                self.excel.getTestCycleKey() + " " + str(start),
                                testModuleIdVal_1,
                                testCaseIdVal_1,
                                testCaseDescriptionVal,
                                "Skipped",
                                start_skipped_scenario,
                                end_skipped_scenario,
                                duration_skipped_scenario
                            )

                            continue

                        if str(runModeVal_2).upper() == "YES":
                            start_time_scenario = time.time()
                            start_scenario = self.stamp.test_start()
                            end_scenario = "n/a"
                            scenario_duration = "n/a"

                            self.sql_exe.validate_test_scen(
                                self.excel.getProjectInfo(),
                                self.excel.getTestCycleKey() + " " + str(start),
                                testModuleIdVal_1,
                                testCaseIdVal_1,
                                testCaseDescriptionVal,
                                str(runModeVal_2).upper(),
                                start_scenario,
                                end_scenario,
                                scenario_duration
                            )

                            testCaseID = testCaseIdVal_1
                            countTestCollection += 1

                            for fromTestStepRow in range(2, test_module_max_row + 1):

                                testCaseIdVal_2 = self.excel.getCellData(testModuleNameVal_1, fromTestStepRow,
                                                                         self.constant.TestCaseIdCol_2)

                                if testCaseID == testCaseIdVal_2:

                                    tc_id = testCaseIdVal_2
                                    start_step = self.stamp.test_start()

                                    testStepIdVal = self.excel.getCellData(testModuleNameVal_1, fromTestStepRow,
                                                                           self.constant.TestStepIdCol)
                                    testStepDescription = self.excel.getCellData(testModuleNameVal_1, fromTestStepRow,
                                                                                 self.constant.TestStepDescription)
                                    actionKeywordVal = self.excel.getCellData(testModuleNameVal_1, fromTestStepRow,
                                                                              self.constant.ActionKeywordCol)
                                    propertyVal = self.excel.getCellData(testModuleNameVal_1, fromTestStepRow,
                                                                         self.constant.PropertyCol)
                                    nObject = self.excel.getCellData(testModuleNameVal_1, fromTestStepRow,
                                                                     self.constant.ObjectCol)
                                    nObject_Value = self.excel.getObjectValue(self.constant.SheetName_objects, nObject)
                                    nDataValue = self.excel.getCellData(testModuleNameVal_1, fromTestStepRow,
                                                                        self.constant.ValueCol)

                                    print("\nTest Case ID: {} - Test Step ID: {} - Test Description: {}".format(
                                        testCaseIdVal_2, testStepIdVal, testStepDescription))

                                    stepResult = self.driver_method.execute_keyword(actionKeywordVal, nDataValue,
                                                                                   nObject_Value)

                                    step_status = ""

                                    if stepResult:
                                        print("Test Step Passed")
                                        step_status = "Passed"
                                        passedStep += 1
                                    else:
                                        print("Test Step Failed")
                                        failedStep += 1
                                        step_status = "Failed"

                                    end_step = self.stamp.test_end()

                                    print("Step: " + start_step)

                                    self.sql_exe.validate_test_step(
                                        testStepIdVal,
                                        testStepDescription,
                                        nDataValue,
                                        self.excel.getProjectInfo(),
                                        self.excel.getTestCycleKey() + " " + str(start),
                                        testModuleIdVal_1,
                                        tc_id,
                                        start_step,
                                        end_step,
                                        step_status
                                    )

                                    if step_status == "Failed":
                                        break

                            print("Passed Step: {}".format(passedStep))
                            print("Failed Step: {}".format(failedStep))

                            if failedStep <= 0:
                                passed_scenario += 1
                                status_scenario = "Passed"
                                print("Scenario Passed: {}".format(testCaseDescriptionVal))

                                passedStep = 0
                                failedStep = 0
                            else:
                                failed_scenario += 1
                                status_scenario = "Failed"
                                print("Scenario Failed: {}".format(testCaseDescriptionVal))

                                passedStep = 0
                                failedStep = 0

                            end_time_scenario = time.time()
                            end_scenario = self.stamp.test_start()
                            scenario_duration = self.stamp.duration(start_time_scenario, end_time_scenario)

                            self.sql_exe.validate_test_scen(
                                self.excel.getProjectInfo(),
                                self.excel.getTestCycleKey() + " " + str(start),
                                testModuleIdVal_1,
                                testCaseIdVal_1,
                                testCaseDescriptionVal,
                                status_scenario,
                                start_scenario,
                                end_scenario,
                                scenario_duration
                            )

                            print("Scenario: "+start_scenario + " - " + end_scenario)
                            print("Scenario Duration: "+scenario_duration)

                if failed_scenario <= 0:
                    status_module = "Passed"
                    print("Module Passed: {}".format(testModuleNameVal_1))

                    passed_scenario = 0
                    failed_scenario = 0
                else:
                    status_module = "Failed"
                    print("Module Failed: {}".format(testModuleNameVal_1))
                    passed_scenario = 0
                    failed_scenario = 0

                end_time_mod = time.time()
                end_module = self.stamp.test_start()
                mod_duration = self.stamp.duration(start_time_mod, end_time_mod)

                self.sql_exe.validate_module(
                    self.excel.getProjectInfo(),
                    testModuleIdVal_1,
                    testModuleNameVal_1,
                    status_module,
                    self.excel.getTestCycleKey() + " " + str(start),
                    start_module,
                    end_module,
                    mod_duration
                )

                print("Module: "+start_module + " - " + end_module)
                print("Duration: "+mod_duration)

        if countTestModule <= 0:
            print("No Test Module Available")
        elif countTestCollection <= 0:
            print("No Test Collection Available")

        end_time = time.time()
        end = self.stamp.test_end()
        cycle_duration = self.stamp.duration(start_time, end_time)

        self.sql_exe.validate_test_cycle(
            self.excel.getTestCycleKey() + " " + str(start),
            self.excel.getTestCycleDef(),
            start,
            end,
            cycle_duration,
            self.excel.getTesterName(),
            self.excel.getEnvironment(),
            self.excel.getTypeOfTesting(),
            "chrome",
            "Browser Version"
        )