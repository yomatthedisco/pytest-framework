__author__ = "Yom Eustaquio"
__copyright__ = "2019, Test Automation Demo"
__version__ = "0.2.8.6"
__maintainer__ = "Yom Eustaquio"
__email__ = "yom.eustaquio@gmail.com"
__status__ = "Dev"


class Constant:
    Path_Chrome_Driver = "E://chromedriver.exe"
    Path_Firefox_Driver = "E://geckodriver.exe"
    Path_IE_Driver = ""

    Path_Screenshot_Passed_Step = "E://test-automation-demo//Screenshots//Passed-Step//"

    Path_Screenshot_Failed_Step = "E://test-automation-demo//Screenshots//Failed-Step//"

    Path_Excel_File_TestCase = "E://test-automation-demo//AutomationTestCases.xlsx"
    SheetName_testModule = "Test Module"
    SheetName_testCollection = "Test Collection"
    SheetName_objects = "Test Objects"
    SheetName_keywords = "Keywords"
    SheetName_projectInfo = "Project Info"

    TestModuleIdCol_1 = 1
    TestModuleNameCol_1 = 2
    RunModeCol_1 = 3

    TestModuleIdCol_2 = 1
    TestModuleNameCol_2 = 2
    TestCaseIdCol_1 = 3
    TestCaseDescription = 4
    RunModeCol_2 = 5

    TestCaseIdCol_2 = 1
    TestStepIdCol = 2
    TestStepDescription = 3
    ActionKeywordCol = 4
    PropertyCol = 5
    ObjectCol = 6
    ValueCol = 7

    ModuleName = 1
    PageObjectNameCol = 2
    Identifier = 3

    ProjectInfoCol = 3
    ProjectInfoInit = 3
    ProjectInfoEnd = 7


