import openpyxl
from Utility.constant import Constant

__author__ = "Yom Eustaquio"
__copyright__ = "2019, Test Automation Demo"
__version__ = "0.2.8.3"
__maintainer__ = "Yom Eustaquio"
__email__ = "yom.eustaquio@gmail.com"
__status__ = "Dev"


class ExcelExec:
    
    def __init__(self):
        self.constant = Constant()
    
    def setExcelFile(self, path):
        try:
            self.workbook = openpyxl.load_workbook(path)
        except:
            print('Failed to open the file: {}'.format(path))
    
    def getSheetName(self, sheetName):
        try:
            sheet = self.workbook[sheetName]
            return sheet
        except:
            print('Failed to get {} sheet'.format(sheetName))

    def getRowCount(self, sheetName):
        try:
            sheet = self.getSheetName(sheetName)
            return sheet.max_row
        except:
            print('Failed to get total row count of {} sheet'.format(sheetName))

    def getColCount(self, sheetName):
        try:
            sheet = self.getSheetName(sheetName)
            return sheet.max_column
        except:
            print('Failed to get total column count {}'.format(sheetName))

    def getCellData(self, sheetName, row_count, col_count):
        try:
            sheet = self.getSheetName(sheetName)
            return sheet.cell(row_count, col_count).value
        except:
            print("Failed to get the cell data from {} sheet".format(sheetName))

    def getObjectValue(self, sheetname, objectname):
        try:
            rowcount = self.getRowCount(sheetname)
            for i in range(1, rowcount+1):
                if objectname == "":
                    break
                elif str(objectname) == str(self.getCellData(sheetname, i, self.constant.PageObjectNameCol)):
                    object_value = str(self.getCellData(sheetname, i, self.constant.Identifier))
                    return object_value
        except:
            print("Failed to get object value from {} sheet".format(sheetname))

    def getProjectInfo(self):
        proj_data = []
        try:
            for i in range(self.constant.ProjectInfoInit, self.constant.ProjectInfoEnd):
                get_proj_data = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol)
                proj_data.append(get_proj_data)

            return proj_data
        except:
            print("Unable to get project information")

    def getTesterName(self):
        for i in range(self.constant.ProjectInfoInit, self.getRowCount(self.constant.SheetName_projectInfo)+1):
            nData = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol-1)

            if (nData == "Automation Tester"):
                tester = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol)
                return tester
        
    def getTestCycleKey(self):
        for i in range(self.constant.ProjectInfoInit, self.getRowCount(self.constant.SheetName_projectInfo)+1):
            nData = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol-1)

            if (nData == "Test Cycle Name"):
                test_cycle = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol)
                return test_cycle

    def getTestCycleDef(self):
        for i in range(self.constant.ProjectInfoInit, self.getRowCount(self.constant.SheetName_projectInfo)+1):
            nData = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol-1)

            if (nData == "Test Cycle Definition"):
                test_def = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol)
                return test_def

    def getTypeOfTesting(self):
        for i in range(self.constant.ProjectInfoInit, self.getRowCount(self.constant.SheetName_projectInfo)+1):
            nData = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol-1)

            if (nData == "Type Of Testing"):
                test_type = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol)
                return test_type

    def getEnvironment(self):
        for i in range(self.constant.ProjectInfoInit, self.getRowCount(self.constant.SheetName_projectInfo)+1):
            nData = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol-1)

            if (nData == "Environment"):
                test_env = self.getCellData(self.constant.SheetName_projectInfo, i, self.constant.ProjectInfoCol)
                return test_env