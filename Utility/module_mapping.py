from Driver.selenium_webdriver import SeleniumDriver

__author__ = "Yom Eustaquio"
__copyright__ = "2019, Test Automation Demo"
__version__ = "0.3.8.7"
__maintainer__ = "Yom Eustaquio"
__email__ = "yom.eustaquio@gmail.com"
__status__ = "Dev"

class DriverMapping(SeleniumDriver):

    # def __init__(self, driver):
    #     super(DriverMapping, self).__init__(driver)

    def execute_keyword(self, keyword, value, objectname):
        if keyword == 'navigate':
            result = None
            result = self.navigate(value)
            return result
        
        elif keyword == 'input':
            result = None
            result = self.sendKeys(value, objectname)
            return result
        
        elif keyword == 'upload':
            result = None
            result = self.upload(value, objectname)
            return result
        
        elif keyword == "click":
            result = None
            result = self.elementClick(value, objectname)
            return result
        
        elif keyword == "scroll":
            result = None
            result = self.webScroll(value)
            return result
        
        elif keyword == "refresh":
            result = None
            result = self.refreshBrowser(value)
            return result
        
        elif keyword == "specialKey":
            result = None
            result = self.specialKey(value, objectname)
            return result