from time import localtime, strftime
import time

class TimeStamp:

    def test_start(self):
        return strftime("%B %d, %Y %H:%M:%S", localtime())

    def test_end(self):
        return strftime("%B %d, %Y %H:%M:%S", localtime())

    def duration(self, start, end):
        e = int(end) - int(start)

        hour = e // 3600
        second = e % 3600 // 60
        split_sec = e % 60

        duration = str(hour) + "hr - " + str(second) + "min - " + str(split_sec) +"sec"

        return duration
