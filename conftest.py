from selenium import webdriver
from Driver.webdriver_factory import GetWebdriverInstance
import pytest

__author__ = "Yom Eustaquio"
__copyright__ = "2019, Test Data Demo"
__version__ = "0.1.7.29"
__maintainer__ = "Yom Eustaquio"
__email__ = "yom.eustaquio@gmail.com"
__status__ = "Dev"


@pytest.fixture(scope="class")
def setup(request, browser):
    wdf = GetWebdriverInstance(browser)
    driver = wdf.getbrowserInstance()

    options = webdriver.ChromeOptions()
    options.add_argument("--disable-infobars")
    options.add_argument("--window-size=800,600")

    options.add_experimental_option("prefs", {
        "profile.default_content_setting_values.media_stream_mic": 1,
        "profile.default_content_setting_values.media_stream_camera": 1,
        "profile.default_content_setting_values.geolocation": 1,
        "profile.default_content_setting_values.notifications": 1,
    })

    if request.cls is not None:
        request.cls.driver = driver
    yield driver
    driver.quit()

def pytest_addoption(parser):
    parser.addoption("--browser")

@pytest.fixture(scope="session")
def browser(request):
    return request.config.getoption("--browser")
